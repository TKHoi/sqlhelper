﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SQLHelperWebDebug.Startup))]
namespace SQLHelperWebDebug
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
