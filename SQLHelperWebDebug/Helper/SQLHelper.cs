﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Transactions;
using System.Text.RegularExpressions;
using System.IO;

namespace SQLHelperWebDebug.Helper
{
    public enum JoinOption
    {
        None = 0,
        Left = 1,
        Right = 2,
        Inner = 3,
        Outer = 4,
        LeftOuter = 5,
        RightOuter = 6,
    }
    public enum SelectOption
    {
        None = 0,
        Distinct = 7,
    }

    public enum LikeOption
    {
        Match = 0,
        Left = 1,
        Right = 2,
    }

    public enum WhereInOption
    {
        None = 0,
        Not = 8,
    }

    public enum UnionOption
    {
        None = 0,
        All = 9,
    }

    public class SQL //Sql Helper version 2.4
    {
        private string ConnectionString { get; set; }
        private SqlConnection connection;
        private string[] sqlOption = new string[] { "", "LEFT ", "RIGHT ", "INNER ", "OUTER ", "LEFT OUTER ", "RIGHTER OUTER ", "DISTINCT ", "NOT ", "ALL " };
        private List<string> sql = new List<string>();
        private bool firstWhere = true;
        private bool firstHaving = true;
        public bool log = false;
        public bool ExecptionDebug = true;
        public string last_query { get; set; }
        private string QueryErrorMsg { get; set; }

        private string NoPrimaryKey = "No primary key";
        public SQL(string ConnectionStringName)
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
        }

        public SQL(string DataSource, string DataBase, string User, string Password)
        {
            ConnectionString = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", DataSource, DataBase, User, Password);
        }

        public SQL(string DataSource, string DataBase) // for Windows Authentication connect db 
        {
            ConnectionString = String.Format("Data Source={0};Initial Catalog={1};persist security info=True; Integrated Security = SSPI;", DataSource, DataBase);
        }

        public string GetConnString()
        {
            return ConnectionString;
        }

        public DataTable getData(string sql = null)
        {
            if (sql == null)
                sql = string.Join(" ", this.sql.ToArray());
            connection = new SqlConnection(ConnectionString);
            DataSet ds = new DataSet();
            using (connection)
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                last_query = sql;
                try
                {
                    da.Fill(ds);
                    clearQueryErrorMsg();
                }
                catch (Exception e)
                {
                    if (ExecptionDebug)
                        throw e;
                    QueryErrorMsg = e.Message;
                    writeQuery(QueryErrorMsg);
                    return null;
                }

            }
            writeQuery(last_query);
            initSql();
            return ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
        }

        public DataSet getAllData(string sql = null)
        {
            if (sql == null)
                sql = string.Join(" ", this.sql.ToArray());
            connection = new SqlConnection(ConnectionString);
            DataSet ds = new DataSet();
            using (connection)
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                last_query = sql;
                try
                {
                    da.Fill(ds);
                    clearQueryErrorMsg();
                }
                catch (Exception e)
                {
                    if (ExecptionDebug)
                        throw e;
                    QueryErrorMsg = e.Message;
                    writeQuery(QueryErrorMsg);
                    return null;
                }

            }
            writeQuery(last_query);
            initSql();
            return ds.Tables.Count > 0 ? ds : new DataSet();
        }

        public List<Dictionary<string, object>> getListDictonary(string sql = null)
        {
            if (sql == null)
                sql = string.Join(" ", this.sql.ToArray());
            connection = new SqlConnection(ConnectionString);
            DataSet ds = new DataSet();
            using (connection)
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                last_query = sql;
                try
                {
                    da.Fill(ds);
                    clearQueryErrorMsg();
                }
                catch (Exception e)
                {
                    QueryErrorMsg = e.Message;
                    writeQuery(QueryErrorMsg);
                    return null;
                }

            }
            writeQuery(last_query);
            initSql();
            var TData = ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            List<Dictionary<string, object>> ResultData = new List<Dictionary<string, object>>();
            foreach (DataRow dr in TData.Rows)
            {
                Dictionary<string, object> tmp = new Dictionary<string, object>();
                foreach (DataColumn col in dr.Table.Columns)
                {
                    tmp.Add(col.ColumnName, dr[col.ColumnName]);
                }
                ResultData.Add(tmp);
            }
            return ResultData;
        }

        public int sqlQuery(string sql = null)
        {
            if (sql == null)
                sql = string.Join(" ", this.sql.ToArray());
            int query = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                last_query = sql;
                try
                {
                    conn.Open();
                    query = cmd.ExecuteNonQuery();
                    conn.Close();
                    clearQueryErrorMsg();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    if (ExecptionDebug)
                        throw ex;
                    QueryErrorMsg = ex.Message.ToString();
                    writeQuery(QueryErrorMsg);
                }
            }
            writeQuery(last_query);
            initSql();
            return query;
        }

        //public int transQuery(string sql = null)
        //{
        //    if (sql == null)
        //        sql = string.Join(" ", this.sql.ToArray());
        //    int query = 0;
        //    using (TransactionScope scope = new TransactionScope())
        //    {
        //        SqlConnection conn = new SqlConnection(ConnectionString);
        //        SqlCommand cmd = new SqlCommand(sql, conn);
        //        last_query = sql;
        //        try
        //        {
        //            conn.Open();
        //            query = cmd.ExecuteNonQuery();
        //            scope.Complete();
        //            clearQueryErrorMsg();
        //        }
        //        catch (Exception ex)
        //        {
        //            if (ExecptionDebug)
        //                throw ex;
        //            QueryErrorMsg = ex.Message;
        //            writeQuery(QueryErrorMsg);
        //        }
        //        finally
        //        {
        //            cmd.Dispose();
        //            conn.Close();
        //            conn.Dispose();
        //        }
        //    }
        //    writeQuery(last_query);
        //    initSql();
        //    return query;
        //}


        public SQL select(string selection = "*", int Top = 0, SelectOption option = 0)
        {
            string parm = sqlOption[(int)option];
            if (Top > 0)
                parm += "TOP " + Top.ToString() + " ";
            sql.Add("SELECT " + parm + " " + selection + " ");
            return this;
        }

        public SQL select(string[] selection, int Top = 0, SelectOption option = 0)
        {
            string parm = sqlOption[(int)option];
            if (Top > 0)
                parm += "TOP " + Top.ToString() + " "; string str = string.Join(",", selection);
            sql.Add("SELECT " + parm + " " + str + " ");
            return this;
        }

        public SQL from(params string[] tableName)
        {
            sql.Add("FROM " + string.Join(",", tableName));
            return this;
        }

        public SQL where(string condition, params object[] value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                if (value[i] == null)
                    value[i] = "";
                condition = condition.Replace("{" + i + "}", ParamExpression(escape(value[i].ToString()), true));
            }
            if (firstWhere)
            {
                sql.Add("WHERE " + condition);
                firstWhere = false;
            }
            else
                sql.Add("AND " + condition);
            return this;
        }

        public SQL where_in(string key, object[] values, WhereInOption option = 0)
        {
            if (values.Length == 0)
                return this;
            string parm = sqlOption[(int)option];
            string str = "";
            str += key + " " + parm + "IN (";
            string[] sql_arr = new string[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] == null)
                    values[i] = "";
                sql_arr[i] = ParamExpression(escape(values[i].ToString()), true);
            }
            string arr = string.Join(",", sql_arr);
            str += arr + ")";
            if (firstWhere)
            {
                sql.Add("WHERE " + str);
                firstWhere = false;
            }
            else
                sql.Add("AND " + str);
            return this;
        }

        public SQL like(string key, object value, LikeOption option = 0)
        {
            string str = key + " LIKE ";
            if (value == null)
                value = "";
            switch (sqlOption[(int)option].Trim())
            {
                case "LEFT":
                    str += ParamExpression("%" + escape(value.ToString()));
                    break;
                case "RIGHT":
                    str += ParamExpression(escape(value.ToString() + "%"));
                    break;
                default:
                    str += ParamExpression("%" + escape(value.ToString() + "%"));
                    break;
            }
            if (firstWhere)
            {
                sql.Add("WHERE " + str);
                firstWhere = false;
            }
            else
            {
                sql.Add("AND " + str);
            }
            return this;
        }

        public SQL like_in(string key, object[] values, LikeOption option = 0, bool or = false)
        {
            string[] tsql = new string[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                var value = values[i];
                string str = key + " LIKE ";
                switch (sqlOption[(int)option].Trim())
                {
                    case "LEFT":
                        str += ParamExpression(escape("%" + value.ToString()));
                        break;
                    case "RIGHT":
                        str += ParamExpression(escape(value.ToString() + "%"));
                        break;
                    default:
                        str += ParamExpression(escape("%" + value.ToString() + "%"));
                        break;
                }
                tsql[i] = str;
            }
            if (firstWhere)
            {
                sql.Add("WHERE (" + string.Join(" OR ", tsql) + ")");
                firstWhere = false;
            }
            else if (or)
            {
                sql.Add("OR (" + string.Join(" OR ", tsql) + ")");
            }
            else
                sql.Add("AND (" + string.Join(" OR ", tsql) + ")");
            return this;
        }

        public SQL join(string tableName, string condition, JoinOption option = 0)
        {
            string parm = sqlOption[(int)option];
            sql.Add(parm + "JOIN " + tableName + " ON " + condition);
            return this;
        }

        public SQL group_by(params string[] column)
        {
            sql.Add("GROUP BY " + string.Join(",", column));
            return this;
        }

        public SQL having(string condition, params string[] value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                condition = condition.Replace("{" + i + "}", ParamExpression(escape(value[i].ToString()), true));
            }

            if (firstHaving)
            {
                sql.Add("HAVING " + condition);
                firstHaving = false;
            }
            else
            {
                sql.Add("AND " + condition);
            }
            return this;
        }

        public SQL order_by(params string[] column)
        {
            string condition = string.Join(",", column);
            sql.Add("ORDER BY " + condition);
            return this;
        }

        public SQL insert(string tableName = null, object InsertData = null)
        {
            var insertKey = InsertData.GetType().GetProperties();
            string[] keys = new string[insertKey.Length];
            string[] values = new string[insertKey.Length];
            for (int i = 0; i < insertKey.Length; i++)
            {
                string theValue = "null";
                keys[i] = insertKey[i].Name;
                if (InsertData.GetType().GetProperty(keys[i]).GetValue(InsertData, null) != null)
                    theValue = InsertData.GetType().GetProperty(keys[i]).GetValue(InsertData, null).ToString();
                values[i] = ParamExpression(theValue);
            }
            sql.Add("INSERT " + " INTO " + tableName + "([" + string.Join("],[", keys) + "]) VALUES" + "(" + string.Join(",", values) + ")");
            return this;
        }

        public int insert_id(string tableName)
        {
            DataTable res = this.select("ISNULL(IDENT_CURRENT('" + tableName.Replace("[", "").Replace("]", "") + "'),-1) as idx").getData();
            if (res.Rows.Count == 0)
                return 0;
            return int.Parse(res.Rows[0]["idx"].ToString());
        }

        public SQL insert_batch(string tableName = null, object[] InsertData = null)
        {
            var insertKey = InsertData[0].GetType().GetProperties();
            string[] keys = new string[insertKey.Length];
            string[] values = new string[InsertData.Length];
            for (int i = 0; i < insertKey.Length; i++)
            {
                keys[i] = insertKey[i].Name;
            }
            for (int i = 0; i < InsertData.Length; i++)
            {
                string[] val = new string[insertKey.Length];
                for (int j = 0; j < keys.Length; j++)
                {
                    string theValue = "null";
                    if (InsertData[i].GetType().GetProperty(keys[j]).GetValue(InsertData[i], null) != null)
                        theValue = InsertData[i].GetType().GetProperty(keys[j]).GetValue(InsertData[i], null).ToString();
                    val[j] = ParamExpression(theValue);
                }
                values[i] = "(" + string.Join(",", val) + ")";
            }
            string sql_str = "INSERT INTO " + tableName + "([" + string.Join("],[", keys) + "]) VALUES ";
            sql_str += string.Join(",", values);
            sql.Add(sql_str);
            return this;
        }

        public SQL update(string tableName, object UpdateData)
        {
            string str = "UPDATE " + tableName + " SET ";
            var UpdateKey = UpdateData.GetType().GetProperties();
            string[] update_string = new string[UpdateKey.Length];
            for (int i = 0; i < UpdateKey.Length; i++)
            {
                string theValue = "null";
                if (UpdateData.GetType().GetProperty(UpdateKey[i].Name).GetValue(UpdateData, null) != null)
                    theValue = UpdateData.GetType().GetProperty(UpdateKey[i].Name).GetValue(UpdateData, null).ToString();
                update_string[i] = UpdateKey[i].Name + "=" + ParamExpression(escape(theValue));
            }
            str += string.Join(",", update_string);
            sql.Add(str);
            return this;
        }

        public SQL update(string tableName, params string[] condition)
        {
            string str = "UPDATE " + tableName + " SET " + string.Join(",", condition);
            sql.Add(str);
            return this;
        }

        public SQL update_batch(string tableName, object[] UpdateData, string conditionKey)
        {
            string sql_str = "UPDATE " + tableName + " SET ";
            var UpdateKey = UpdateData[0].GetType().GetProperties();
            Dictionary<string, List<string>> Update_List = new Dictionary<string, List<string>>();
            for (int i = 0; i < UpdateData.Length; i++)
            {
                string[] condition = new string[UpdateKey.Length];
                for (int j = 0; j < UpdateKey.Length; j++)
                {
                    if (!Update_List.ContainsKey(UpdateKey[j].Name))
                        Update_List.Add(UpdateKey[j].Name, new List<string>());
                    string theValue = "null";
                    if (UpdateData[i].GetType().GetProperty(UpdateKey[j].Name).GetValue(UpdateData[i], null) != null)
                        theValue = UpdateData[i].GetType().GetProperty(UpdateKey[j].Name).GetValue(UpdateData[i], null).ToString();
                    Update_List[UpdateKey[j].Name].Add(ParamExpression(theValue));
                }
            }
            List<string> finalSql = new List<string>();
            foreach (KeyValuePair<string, List<string>> Update in Update_List)
            {
                if (Update.Key == conditionKey)
                    continue;
                string[] tmp = new string[Update_List[conditionKey].Count];
                for (int i = 0; i < Update_List[conditionKey].Count; i++)
                {
                    tmp[i] = " WHEN " + Update_List[conditionKey][i] + " THEN " + Update.Value[i];
                }
                finalSql.Add(Update.Key + " = ( CASE " + conditionKey + string.Join(" ", tmp) + " ELSE " + Update.Key + " END )");
            }
            sql.Add(sql_str + string.Join(",", finalSql.ToArray()));
            return this;
        }
        public SQL delete(string table)
        {
            sql.Add("DELETE FROM " + table);
            return this;
        }

        public SQL insert_from_select(string insertTable, params string[] insert_key)
        {
            sql.Add("INSERT " + " INTO " + insertTable + "(" + string.Join(",", insert_key) + ")");
            return this;
        }

        public SQL union(string sql1, string sql2, UnionOption option = 0)
        {
            sql.Add("(" + sql1 + ") UNION " + sqlOption[(int)option] + "(" + sql2 + ")");
            return this;
        }

        public string escape(string usString)
        {
            if (usString is System.DBNull)
            {
                return "";
            }
            else
            {
                string sample = Convert.ToString(usString);
                sample = sample.Replace("'", "''").Replace("\\", "/");
                return sample.Trim();
            }
        }

        public string varchar(string str)
        {
            return "'" + escape(str) + "'";
        }

        public string Nvarchar(string str)
        {
            return "N'" + escape(str) + "'";
        }

        public string ShowQueryErrorMsg()
        {
            string tmp = this.QueryErrorMsg;
            this.QueryErrorMsg = null;
            return tmp;
        }

        public void initSql()
        {
            if (sql.Count > 0)
                sql.Clear();
            firstWhere = firstHaving = true;
        }

        public string showReadyQuery(string inpSql = null)
        {
            if (inpSql != null)
                return inpSql;
            return string.Join(" ", sql.ToArray());
        }

        public string GetReadyQuery()
        {
            string s = string.Join(" ", sql.ToArray());
            initSql();
            return s;
        }

        public void clearQueryErrorMsg()
        {
            this.QueryErrorMsg = null;
        }

        public void writeQuery(string sql = null) //本功能僅用於Debug
        {
            if (!log)
                return;
            else if (sql != null)
            {
                try
                {
                    string folder_path = AppDomain.CurrentDomain.BaseDirectory + @"Log";
                    if (!Directory.Exists(folder_path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(folder_path);
                    }					
                    string path = AppDomain.CurrentDomain.BaseDirectory + @"Log\QueryLog_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".txt";
                    if (!File.Exists(path))
                    {
                        using (StreamWriter sw = File.CreateText(path))
                        {
                            sw.WriteLine(sql);
                        }
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(path, true))
                        {
                            sw.WriteLine(sql);
                        }
                    }
                }
                catch (Exception e) { return; }
                return;
            }
        }

        //private string getPrimaryKeyName(string table)
        //{
        //    string sql = @"SELECT Col.Column_Name from
        //                    INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab,
        //                    INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col
        //                    WHERE
        //                        Col.Constraint_Name = Tab.Constraint_Name
        //                        AND Col.Table_Name = Tab.Table_Name
        //                        AND Constraint_Type = 'PRIMARY KEY'
        //                        AND Col.Table_Name = " + varchar(table.Replace("[", "").Replace("]", ""));
        //    DataTable res = getData(sql);
        //    if (res.Rows.Count == 0)
        //        return NoPrimaryKey;
        //    return res.Rows[0]["Column_Name"].ToString();
        //}

        public string ParamExpression(string input, bool NotNvarChar = false)
        {
            if (input == null)
                return input;
            var regx_num = @"^(0|[1-9][0-9]+)$";
            var regx_point_num = @"^(0|[1-9][0-9]+)\.[0-9]+$";
            var regx_en_num = @"[\u4e00-\u9fa5]$";
            if (Regex.Match(input, regx_num).Success || Regex.Match(input, regx_point_num).Success)
                return input;
            if (NotNvarChar)
                return varchar(input);
            return Nvarchar(input);
        }

        public string CombRowInCol(string col, string table, string condition, string combSymbol = ",")
        {
            return "STUFF((SELECT " + varchar(combSymbol) + "+" + col + " FROM " + table + " WHERE " + condition + " FOR XML Path('')),1,1,'')";
        }

        public void trans_end(TransactionScope scope, bool isComplete)
        {
            if (isComplete)
                scope.Complete();
            else
                scope.Dispose();
        }
    }
}